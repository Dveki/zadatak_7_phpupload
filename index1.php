<!DOCTYPE HTML>  
<html>
<head>
<link rel="stylesheet" href="style1.css">
</head>
<body>  

<?php
    $name = $lname = $dirchoice = $file = "";
    $nameErr = $lnameErr = $dirchoiceErr = $fileErr = "";
    
    if (isset($_GET['name'])) { $name = $_GET['name']; }
    if (isset($_GET['lname'])) { $lname = $_GET['lname']; }
    if (isset($_GET['dirchoice'])) { $dirchoice = $_GET['dirchoice']; }
    if (isset($_GET['file'])) { $file = $_GET['file']; }
    

    if (isset($_GET['nameErr'])) { $nameErr = $_GET['nameErr']; }
    if (isset($_GET['lnameErr'])) { $lnameErr = $_GET['lnameErr']; }
    if (isset($_GET['dirchoiceErr'])) { $dirchoiceErr = $_GET['dirchoiceErr']; }
    if (isset($_GET['fileErr'])) { $fileErr = $_GET['fileErr']; }
     
?>
<div class="main">
<form method="post" name="upload" action="myPhoto.php" enctype="multipart/form-data">
<fieldset class="main_field">
<h2>Upload photo</h2>
<p><span class="error">* required field.</span></p>
  Name: <input type="text" name="name" value="<?php echo $name;?>">
  <span class="error">* <?php echo $nameErr;?></span>
  <br><br>
  Last Name: <input type="text" name="lname" value="<?php echo $lname;?>">
  <span class="error">* <?php echo $lnameErr;?></span>
  <br><br>
  
  Directory:
  <input type="radio" name="dirchoice" <?php if (isset($dirchoice) && $dirchoice=="private") echo "checked";?> value="private">Private
  <input type="radio" name="dirchoice" <?php if (isset($dirchoice) && $dirchoice=="public") echo "checked";?> value="public">Public
  <span class="error">* <?php echo $dirchoiceErr;?></span>
  <br><br>
<label for="if">File:</label>
<input type="file" name="file" id="if"/>
<span class="error">* <?php echo $fileErr;?></span><br /><br />
<!--<input type="submit" name="sb" id="sb" value="upload" />-->
<input type="reset" name="rb" id="rb" value="cancel" />
</fieldset>
  <input type="submit" name="submit" value="Submit/Upload">  
</form>
</div>

</body>
</html>