<!DOCTYPE HTML>  
<html>
<head>
</head>
<body>  

<?php
// define variables and set to empty values
$nameErr = $lnameErr = $dirchoiceErr = $fileErr = "";
$name = $lname = $dirchoice = $file ="";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $nameErr = "Name is required";
  } else {
    $name = test_input($_POST["name"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $nameErr = "Only letters and white space allowed"; 
    }
    elseif(strlen($_POST["name"]) < 3  ) {
        $nameErr = "Name need to have more than 2 characters";
      }
      elseif(strlen($_POST["name"]) > 10  ) {
        $nameErr = "Name need to have less than 10 characters";
      }
      else {
        $name = test_input($_POST["name"]);
      }
  }

  if (empty($_POST["lname"])) {
    $lnameErr = "Last Name is required";
  } else {
    $lname = test_input($_POST["lname"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$lname)) {
      $lnameErr = "Only letters and white space allowed"; 
    }
    elseif(strlen($_POST["lname"]) < 3  ) {
        $lnameErr = "Last Name need to have more than 2 characters";
      }
      elseif(strlen($_POST["lname"]) > 10  ) {
        $lnameErr = "Last Name need to have less than 10 characters";
      }
      else {
        $lname = test_input($_POST["lname"]);
      }
  }
  
  if (empty($_POST["dirchoice"])) {
    $dirchoiceErr = "Directory is required";
  } else {
    $dirchoice = test_input($_POST["dirchoice"]);
  }

  if (empty($_POST["file"])) {
    $fileErr = "Photo is required";
  }
  if (isset($_FILES["file"]) AND is_uploaded_file($_FILES['file']['tmp_name'])) {
    $file_name = $_FILES['file']["name"];
    $file_temp = $_FILES["file"]["tmp_name"];
    $file_size = $_FILES["file"]["size"];
    $file_type = $_FILES["file"]["type"];
    $file_error = $_FILES['file']["error"];
    if ($file_error >0) {
        echo "Something went wrong during file upload!";
    }
    else {
        // http://en.wikipedia.org/wiki/Exchangeable_image_file_format
        // http://www.php.net/manual/en/book.exif.php
        //echo exif_imagetype($file_temp)."<br />";
        if (!exif_imagetype($file_temp)) {
          $fileErr = "File is not a picture!";
        }
        elseif (exif_imagetype($file_temp) != IMAGETYPE_JPEG) {
          $fileErr = "Not a JPG/JPEG image!";   
           }
        
        if (exif_imagetype($file_temp) == IMAGETYPE_JPEG) {
          $fileErr = "Your photo $file_name is saved to directory: " .ucfirst($_POST["dirchoice"])."!";
       /*echo "Ime poslate datoteke : $file_name <br />";
         echo "Privremena lokacija datoteke / Az ideiglenes fájl eljárási útja: $file_temp <br />";
        echo "Veličina poslate datoteke u bajtovima / A feltöltött fájl mérete bájtban: $file_size <br />";
        echo "Tip poslate datoteke / A feltöltött fájl típusa: $file_type <br />";
        echo "Kod greške / Hibakód: $file_error <br />";*/
        $ext_temp = explode(".", $file_name);
        $extension = end($ext_temp);
        
        $file_name_name = "slika";
        $new_file_name = strtolower($_POST['name']).strtolower($_POST['lname']). "-" .date("YmdHis") . "-" . rand(1,10) . ".$extension";
        
        $directory = ($_POST["dirchoice"]);
        $upload = "$directory/$new_file_name"; // images/20171110084338.jpg
        // upload fajla
        if (!is_dir($directory)) {
          mkdir($directory);
        }
        if (!file_exists($upload)) {
            if (move_uploaded_file($file_temp, $upload)) {
                $size = getimagesize($upload);
                //var_dump($size);
                foreach ($size as $key => $value)
                echo "$key = $value<br />";
                echo "<img src=\"$upload\" $size[3] border=\"0\" alt=\"$file_name\" />";
            }
            else {
              echo "<p><b>Error!</b></p>";
            }
        }
        /*else {
            for($i=1; $i<9; $i++){
                if(!file_exists($directory.'/'.$file_name_name.$i.'.'.$extension)){
                  move_uploaded_file($file_temp, $directory.'/'.$file_name_name.$i.'.'.$extension);
                  break;
                }
                else{
                  echo "<p><b>File with this name already exists!</b></p>";
                }*/
            }
        }
    }
    

  if (!empty($nameErr) or !empty($lnameErr) or !empty($dirchoiceErr) or !empty($fileErr)) {
    $params .= "name=" . urlencode($_POST["name"]);
    $params .= "&lname=" . urlencode($_POST["lname"]);
    $params .= "&dirchoice=" . urlencode($_POST["dirchoice"]);
    $params .= "&file=" . urlencode($_POST["file"]);
    
    $params .= "&nameErr=" . urlencode($nameErr);
    $params .= "&lnameErr=" . urlencode($lnameErr);
    $params .= "&dirchoiceErr=" . urlencode($dirchoiceErr);
    $params .= "&fileErr=" . urlencode($fileErr);
    
    
    header("Location: index1.php?" . $params);
  }  
  /*else {
    
    echo "<h2>Your Input:</h2>";
    echo "Full Name: " .ucfirst($_POST['name']).' '.ucfirst($_POST['lname']);
    echo "<br>";
    
    
    echo "Directory is: " . $_POST['dirchoice'];  
    echo "<br>";
    echo "<br>";


  }*/

  echo "<a href=\"index1.php\">Return to form</a>";
  }



function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

?>

</body>
</html>